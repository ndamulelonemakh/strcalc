using System;
using System.Collections.Generic;

namespace StringCalculator {
    public class utils {

        public static int arraySum (int[] numbersList) {
            int sum = 0;
            foreach (int num in numbersList) {
                sum = sum + num;
            }
            return sum;
        }

        // Case 2 - Specified delimeters
        public static int[] detect_numbers (string[] stringNumbers, string[] delimeterList) {
            List<int> numList = new List<int> ();
            List<int> numListCopy = new List<int> ();
            numList.Add ( Int32.Parse( stringNumbers[0] ) ); //default entry

            for (int i = 0; i < delimeterList.Length; i++) {
                foreach (string item in stringNumbers) {
                    if (item.Contains (delimeterList[i])) {
                        foreach (string subitem in item.Split (delimeterList[i])) {
                            numList.Add (Int32.Parse (subitem));
                        }
                    }
                }
            }
            if (numList.ToArray ().Length == 1) {
                foreach (string item in stringNumbers) {
                    numListCopy.Add (Int32.Parse (item));
                }
                numList = numListCopy;
            }
            return numList.ToArray ();
        }

        public static (string, int) detect_delimeter (string numString, int _delim_char_index) {
            string delimeter = "";
            int _index = _delim_char_index;

            while (numString.Substring(_index, length:1 ) != "]") {
                delimeter = delimeter + numString.Substring(_index, length:1);
                _index++;
            }
            return (delimeter, _index);
        }

        public static string[] delimeter_list (string numString, int _delim_char_index) {
            bool isNextDelimeter = true;
            List<string> delimeter_list = new List<string> ();
            (string delimeter, int lastIndex) = detect_delimeter (numString, _delim_char_index);
            delimeter_list.Add (delimeter);

            // Get next delimeter if it exists
            while (isNextDelimeter) {
                if (numString.Substring(lastIndex + 1, length:1) == "[") {
                    (delimeter, lastIndex) = detect_delimeter(numString, lastIndex + 2);
                    delimeter_list.Add(delimeter);

                }
                else {
                    isNextDelimeter = false;
                }
            }

            return delimeter_list.ToArray ();

        }

        // Case 1 - comma and space delimeters
        public static int[] convert_to_int (string numString) {
            List<int> numList = new List<int> ();
            string[] numStringItems = numString.Split (',');
            Console.WriteLine (numString);
            if (numStringItems.Length == 0 || numString == "") {
                numList.Add(0);
                return numList.ToArray ();
            }
            foreach (string item in numStringItems) {

                if (item.Contains ("\n")) {
                    foreach (string subitem in item.Split ('\n')) {
                        numList.Add (Int32.Parse (subitem));
                    }
                    continue;
                }

                numList.Add (Int32.Parse (item));

                //append items to numList
            }

            return numList.ToArray ();
        }



        // Helpers
        public static void printArray(string[] arr){
            foreach(string item in arr){
                Console.WriteLine(item);
            }
        }
    }
}