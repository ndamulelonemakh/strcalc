using System;
// using Microsoft.VisualStudio.TestTools.UnitTesting ;
using StringCalculator;
using Xunit;

namespace TestCases {
    public class UnitTest1 {
        private readonly Program _app;
        private readonly utils helpers;

        public UnitTest1 () {
            _app = new Program ();
            helpers = new utils ();
        }

        [Fact]
        public void TestEmptyString () {
            var result = Program.findStringNumbersSum ("");
            Assert.Equal (0, result);
            // Expected vs actual
        }

        [Theory]
        [InlineData ("1,2")]
        [InlineData ("2,1")]
        [InlineData ("3")]
        [InlineData ("1,1,1")]
        public void TestCommaDelimeter (string val) {
            var result = Program.findStringNumbersSum (val);
            Assert.Equal (3, result);
            // Check if all digits are identified correctly
            // Irregardless of order or length
        }

        [Fact]
        public void TestCommaSpaceDelimeter () {
            var result = Program.findStringNumbersSum ("1\n2,3");
            Assert.Equal (6, result);
            // Expected vs actual
        }

        [Theory]
        [InlineData ("//[*][%]\n1*2%3")]
        [InlineData ("//[**][%%]\n1**2%%3")]
        [InlineData ("//[;]\n1;2;3")]
        [InlineData ("//[***][%%]\n1***2%%3")]
        [InlineData ("//[&]\n1&2&3")]
        [InlineData ("//[%%%]\n1%%%2%%%3")]
        public void TestMultipleDelimeters(string val) {
            var result = Program.findStringNumbersSum (val);
            Assert.Equal (6, result);
            // Test multple and single delimeters
        }
    }
}