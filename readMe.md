# String Calculator

* This is a sample string calculator app written in python and
translated to **C#**

* If you are using a unix environment 
* Clone project
* Navigate to strcalc > C#
* Use **dotnet run** to run the sample c# console app

* Similarly, to test the application, use **dotnet test** from the **TestCases sub-folder**
