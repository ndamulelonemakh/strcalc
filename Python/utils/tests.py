import unittest
import app
 
class TestAdd(unittest.TestCase):
 
    def test_empty_str(self):
        self.assertEqual( app.string_number_sum(""), 0)

    def test_numbers_comma_delim(self):
        self.assertEqual( app.string_number_sum("1,2"), 3)


    def test_white_spaces(self):
        self.assertEqual( app.string_number_sum("1\n3,2"), 6)


    def test_empty_string(self):
        self.assertEqual( app.string_number_sum("0"), 0)


    def test_greater_than_1000(self):
        self.assertEqual( app.string_number_sum("1000,2"), 2)
    

    def test_negative_values(self):
        with self.assertRaises(ValueError):
            app.string_number_sum("-10,2")

    # Phase 2
    def test_changed_delimeter(self):
        self.assertEqual( app.string_number_sum('//[;]\n1;2'), 3 )

    def test_variable_length_delimters_case_1(self):
        self.assertEqual( app.string_number_sum('//[***]\n1***2***3'), 6)

    def test_multiple_delimters_onechar(self):
        self.assertEqual( app.string_number_sum('//[*][%]\n1*2%3'), 6)

    def test_multiple_delimters_mulchar(self):
        self.assertEqual( app.string_number_sum('//[***][%%]\n1***2%%3'), 6)
        return unittest.TestCase.shortDescription(self)

 
if __name__ == '__main__':
    unittest.main()