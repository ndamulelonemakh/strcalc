import calc_utils

# Manual test variables
test_string = '//[*][%]\n1*2%3'
test_string = '//[***]\n1***2***3'
test_string = "1\n3,2"
# test_string = "1\n,2"
starting_index = 3

def string_number_sum(test_string):
    """
    Takes a number string input
    returns the sum of the numbers e.g."//[,]\n1,2,3" will return 6
    """
    if( "[" in test_string ):
        delimeter_list = calc_utils.get_delimeter_list(test_string, starting_index) 
        
        print('Preprocessing string...with delimeters {0}'.format(delimeter_list))
        string_number_list = test_string.split(delimeter_list[0])

        # Remove leading characters before fist number
        string_number_list = string_number_list[1:] 
        string_number_list[0] = string_number_list[0].split('\n')[1]
        print('The final preprocessed string is {}'.format(string_number_list))

        print('Loading numbers list...')
        numbers_list = calc_utils.get_number_list(string_number_list , delimeter_list)
        print('Detected number list: {0}'.format(numbers_list))

        print('Calculating sum...')
        computed_sum = calc_utils.compute_sum(numbers_list)
        print('Sum of {0} is {1}'.format(numbers_list, computed_sum))
        return computed_sum
    else:
        computed_sum = calc_utils.compute_sum_(test_string)
        return computed_sum

if __name__ == '__main__':
    string_number_sum(test_string)