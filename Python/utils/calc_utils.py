
# Phase 1
def compute_sum_(*args):
    numbers = convert_to_int(*args)
    numbers_sum = add_numbers(numbers)
    print("The sum of {0} is {1}".format(numbers, numbers_sum))
    return numbers_sum


def add_numbers(number_list):
    total = 0
    for num in number_list:
        # Remove numbers greater than 1000 or negative
        if(num>100):
            continue

        if(num < 0):
            raise ValueError('Negative number {0} not allowed!'.format(num));
        total = total + num
    return total


def convert_to_int(*args):
    num_list = []
    arg_list = []

    for arg in args:
        for item in arg.split(','):
            if(item == ''):
                num_list.append(0)
                return num_list

            if( '\n' in item):  # detect space delimeters
                for subitem in item.split('\n'):
                    print(subitem)
                    num_list.append(int(subitem.strip()))
                continue
            num_list.append(int( item.strip().replace('\n','') ))
    return num_list


# Phase 2
def compute_sum(number_list):
    total = 0
    for num in number_list:
        # Remove numbers greater than 1000 or negative
        if(int(num) > 100):
            continue

        if( int(num) < 0):
            raise ValueError('Negative number {0} not allowed!'.format( int(num) ));
        total = total + int(num)
    return total

def get_number_list(string_number_list, delimeter_list):
    numbers_list = []
    numbers_list.append(string_number_list[0]) # Default entry for input pattern //[delim]\n[numbers]

    for i in range( len(delimeter_list) ):
        for item in string_number_list:
            if( delimeter_list[i] in item ):
                for sub_item in item.split( str(delimeter_list[i]) ):
                    numbers_list.append(sub_item)
    if(len(numbers_list) == 1): # If remaining numbers dont have delimeters
        numbers_list = string_number_list
    return numbers_list
        

def detect_delimter(num_string, _delim_char_index):
    delimeter = ""
    
    while( num_string[_delim_char_index] != "]"):
        delimeter = delimeter + num_string[_delim_char_index]
        _delim_char_index = _delim_char_index + 1
    print("Returned delimeter, {0}, and last indexed at {1}".format(delimeter, _delim_char_index - 1 ))
    return delimeter, _delim_char_index

def get_delimeter_list( num_string, _index_):
    isNextDelimeter = True
    delimeter_list = []
    delimeter, _last_index = detect_delimter( num_string, _index_ )
    delimeter_list.append(delimeter)

    # Get next delimeter if exists
    while(isNextDelimeter):
        if( num_string[_last_index + 1] == "[" ):
            delimeter, _last_index = detect_delimter( num_string, _last_index + 2 )
            delimeter_list.append(delimeter)
        else:
            isNextDelimeter = False
            return delimeter_list